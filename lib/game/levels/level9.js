ig.module(
	'game.levels.level9'
)
.requires(
	'game.levels.9.1',
	'game.levels.9.2',
	'game.levels.9.3',
	'game.levels.9.4'
)
.defines(function(){
	level9 = {
		floors: [
			level91,
			level92,
			level93,
			level94
		]
	};
});

ig.module(
	'game.levels.6.4'
)
.defines(function(){
	level64 = {
		"entities": [
			{type: "Exit", x: 64, y: 80},
			{type: "Trap", x: 32, y: 32},
			{type: "Gun", x: 16, y: 16, settings: {axis: 'y', direction: 'right'}}
		],
		"layer": [
			{
				"data": [
					[0, 0, 0, 0, 0, 0, 0],
					[0, 1, 1, 1, 1, 4, 0],
					[0, 3, 1, 13, 3, 13, 0],
					[0, 3, 8, 11, 1, 1, 0],
					[0, 13, 13, 3, 13, 3, 0],
					[0, 13, 1, 3, 1, 1, 0],
					[0, 0, 0, 0, 0, 0, 0]
				]
			}
		]
	};
});

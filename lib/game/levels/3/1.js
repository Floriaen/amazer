ig.module(
	'game.levels.3.1'
)
.defines(function(){
	level31 = {
		"entities": [
			{type: "Spikes", x: 16, y: 64},
			{type: "Exit", x: 32, y: 32},
			{type: "Man", x: 16, y: 16}
		],
		"layer": [
			{
				"data": [
					[0, 0, 0, 0, 0, 0, 0],
					[0, 14, 3, 3, 3, 3, 0],
					[0, 1, 1, 3, 3, 3, 0],
					[0, 1, 1, 1, 5, 3, 0],
					[0, 1, 3, 3, 2, 3, 0],
					[0, 1, 1, 8, 1, 1, 0],
					[0, 0, 0, 0, 0, 0, 0]
				]
			}
		]
	};
});

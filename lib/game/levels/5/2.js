ig.module(
	'game.levels.5.2'
)
.defines(function(){
	level52 = {
		"layer": [
			{
				"data": [

					[0, 0, 0, 0, 0, 0, 0],
					[0, 3, 3, 3, 3, 3, 0],
					[0, 3, 2, 1, 2, 3, 0],
					[0, 3, 4, 3, 3, 1, 0],
					[0, 3, 3, 13, 3, 1, 0],
					[0, 2, 1, 1, 1, 11, 0],
					[0, 0, 0, 0, 0, 0, 0]
					/*
					[0, 0, 0, 0, 0, 0, 0],
					[0, 1, 1, 1, 1, 1, 0],
					[0, 1, 1, 1, 2, 1, 0],
					[0, 1, 1, 1, 1, 1, 0],
					[0, 1, 1, 1, 1, 1, 0],
					[0, 1, 1, 1, 1, 1, 0],
					[0, 0, 0, 0, 0, 0, 0]

					[0, 0, 0, 0, 0, 0, 0],
					[0, 1, 1, 1, 1, 1, 0],
					[0, 1, 1, 1, 1, 1, 0],
					[0, 1, 1, 1, 1, 1, 0],
					[0, 1, 1, 1, 1, 1, 0],
					[0, 2, 1, 1, 1, 1, 0],
					[0, 0, 0, 0, 0, 0, 0]
					*/
				]
			}
		]
	};
});

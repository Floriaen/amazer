ig.module(
	'game.levels.level0'
)
.requires(
	'game.levels.0.1',
	'game.levels.0.2'
)
.defines(function(){
	level0 = {
		floors: [
			level01,
			level02
		]
	};
});

ig.module(
	'game.levels.level10'
)
.requires(
	'game.levels.10.1',
	'game.levels.10.2',
	'game.levels.10.3',
	'game.levels.10.4'
)
.defines(function(){
	level10 = {
		floors: [
			level101,
			level102,
			level103,
			level104
		]
	};
});

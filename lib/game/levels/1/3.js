ig.module(
	'game.levels.1.3'
)
.defines(function(){
	level13 = {
		"entities": [
			{type: "Fire", x: 80, y: 16}
		],
		"layer": [
			{
				"data": [
					[0, 0, 0, 0, 0, 0, 0],
					[0, 3, 3, 3, 3, 1, 0],
					[0, 3, 2, 3, 2, 1, 0],
					[0, 3, 1, 3, 3, 1, 0],
					[0, 3, 3, 1, 3, 1, 0],
					[0, 2, 1, 1, 3, 2, 0],
					[0, 0, 0, 0, 0, 0, 0]
				]
			}
		]
	};
});

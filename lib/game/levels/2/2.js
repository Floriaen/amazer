ig.module(
	'game.levels.2.2'
)
.defines(function(){
	level22 = {
		"entities": [
			{type: "Spikes", x: 32, y: 32},
			{type: "Exit", x: 48, y: 48}
		],
		"layer": [
			{
				"data": [
					[0, 0, 0, 0, 0, 0, 0],
					[0, 3, 1, 1, 3, 2, 0],
					[0, 1, 1, 3, 3, 1, 0],
					[0, 1, 3, 1, 3, 3, 0],
					[0, 3, 3, 1, 1, 3, 0],
					[0, 5, 3, 3, 2, 5, 0],
					[0, 0, 0, 0, 0, 0, 0]
				]
			}
		]
	};
});

ig.module(
	'game.levels.level7'
)
.requires(
	'game.levels.7.1',
	'game.levels.7.2',
	'game.levels.7.3',
	'game.levels.7.4'
)
.defines(function(){
	level7 = {
		floors: [
			level71,
			level72,
			level73,
			level74
		]
	};
});

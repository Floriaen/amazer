ig.module(
	'game.levels.7.1'
)
.defines(function(){
	level71 = {
		"entities": [
			{type: "Man", x: 16, y: 16}
		],
		"layer": [
			{
				"data": [
					[0, 0, 0, 0, 0, 0, 0],
					[0, 1, 1, 3, 3, 3, 0],
					[0, 11, 1, 3, 1, 5, 0],
					[0, 11, 1, 1, 3, 1, 0],
					[0, 11, 1, 1, 1, 3, 0],
					[0, 1, 1, 3, 1, 2, 0],
					[0, 0, 0, 0, 0, 0, 0]
				]
			}
		]
	};
});

ig.module(
	'game.levels.8.2'
)
.defines(function(){
	level82 = {
		"entities": [
			{type: "Spikes", x: 48, y: 48}
		],
		"layer": [
			{
				"data": [
					[0, 0, 0, 0, 0, 0, 0],
					[0, 1, 1, 3, 1, 1, 0],
					[0, 1, 3, 1, 3, 1, 0],
					[0, 3, 5, 1, 5, 3, 0],
					[0, 1, 3, 2, 3, 1, 0],
					[0, 1, 1, 3, 1, 1, 0],
					[0, 0, 0, 0, 0, 0, 0]
				]
			}
		]
	};
});

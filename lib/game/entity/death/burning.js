ig.module(
	'game.entity.death.burning',
	'game.entity.fire'
)
.requires(
	'game.engine.map-entity'
)
.defines(function(){

	Burning = MapEntity.extend({
		
		animSheet: new ig.AnimationSheet('media/death/burning.png', 16, 36),
		sfx: new ig.Sound('media/sfx/BURN_3.*', false),
	
		init: function(x, y, settings) {
			this.parent(x, y, settings);

			this.sfx.volume = 0.6;

			this.pos.x -= 4;
			this.pos.y -= 22;

			this.size.x = 16;
			this.size.y = 36;

			
			this.addAnim('burning', 0.2, [0, 1, 2, 3, 1, 2, 3], true);
			this.addAnim('ghostApparition', 0.18, [4, 5, 6, 7, 8, 9, 10], true);
			this.onComplete = settings.onComplete || null;

			this.sfx.play();
		},

		update: function() {
			this.parent();
			if (this.isCurrentAnim('burning') && this.doesAnimEnd('burning')) {
				this.setAnim('ghostApparition');
				this.currentAnim.rewind();
			}
			if (this.isCurrentAnim('ghostApparition') && this.doesAnimEnd('ghostApparition')) {
			//	var fire = ig.game.spawnEntity('Fire', this.tile.x * ig.global.TILE_SIZE, (this.tile.y + 1) * ig.global.TILE_SIZE);
			//	fire.z = this.z;
			//	fire.setVisible(true);
				ig.game.gameOver();
				this.kill();

				if (this.onComplete) this.onComplete();
			}
		}
	});

});
